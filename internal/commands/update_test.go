package commands

import (
	"flag"
	"io"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/release-cli/internal/flags"
	"gitlab.com/gitlab-org/release-cli/internal/gitlab"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
)

func TestUpdate(t *testing.T) {
	logger, hook := testlog.NewNullLogger()

	mockClient := &gitlab.MockHTTPClient{}
	mockClient.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseGetReleaseSuccess](), nil).Once()

	defer mockClient.AssertExpectations(t)

	update, ctx := newTestUpdate(t, logrus.NewEntry(logger), mockClient, "--name", t.Name(), "--description", t.Name(), "--tag-name", t.Name())
	require.Equal(t, "update", update.Name)
	require.Len(t, update.Flags, 5)

	err := update.Run(ctx)
	require.NoError(t, err)

	for _, entry := range hook.AllEntries() {
		require.Contains(t, []string{"Updating Release...", "file does not exist, using string value for --description", "Release updated successfully!"}, entry.Message)
	}
}

func TestUpdateMilestonesAndReleasedAt(t *testing.T) {
	logger, _ := testlog.NewNullLogger()

	baseArgs := []string{"--name", t.Name(), "--description", t.Name(), "--tag-name", t.Name()}

	tests := []struct {
		name           string
		extraArgs      []string
		expectedRunErr bool
		expectedErrMsg string
	}{
		{
			name:      "success_milestone_and_released_at",
			extraArgs: append([]string{"--milestone", "v1.0", "--released-at", "2022-05-30T21:10:23.284Z"}, baseArgs...),
		},
		{
			name:      "success_multiple_milestones",
			extraArgs: append([]string{"--milestone", "v1.0", "--milestone", "v1.0-rc"}, baseArgs...),
		},
		{
			name:      "success_released_at",
			extraArgs: append([]string{"--released-at", "2022-05-30T21:10:23.284Z"}, baseArgs...),
		},
		{
			name:           "invalid_released_at",
			extraArgs:      append([]string{"--released-at", "2019/01/03 01:55:18"}, baseArgs...),
			expectedErrMsg: "parse released-at: parsing time",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockClient := gitlab.MockHTTPClient{}
			defer mockClient.AssertExpectations(t)
			if tt.expectedErrMsg == "" {
				mockClient.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseGetReleaseSuccess](), nil).Once()
			}

			update, ctx := newTestUpdate(t, logrus.NewEntry(logger), &mockClient, tt.extraArgs...)
			err := update.Run(ctx)
			if tt.expectedErrMsg != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.expectedErrMsg)
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUpdateMissingTagName(t *testing.T) {
	logger, _ := testlog.NewNullLogger()

	mockClient := gitlab.MockHTTPClient{}
	defer mockClient.AssertExpectations(t)

	update, ctx := newTestUpdate(t, logrus.NewEntry(logger), &mockClient, "--name", t.Name(), "--description", t.Name())
	err := update.Run(ctx)

	require.Error(t, err)
	require.Contains(t, err.Error(), `Required flag "tag-name" not set`)
}

func Test_newUpdateReleaseReq(t *testing.T) {
	logger, _ := testlog.NewNullLogger()

	releasedAt, err := gitlab.ParseDateTime("2022-05-30T21:10:23.284Z")
	require.NoError(t, err)

	baseReq := gitlab.UpdateReleaseRequest{
		ID:          "",
		Name:        "name",
		Description: "desc",
		TagName:     "tag",
		Milestones:  []string{"m1", "m2"},
		ReleasedAt:  &releasedAt,
	}

	requiredArgs := []string{"--tag-name", "tag"}

	tests := []struct {
		name           string
		args           []string
		expected       *gitlab.UpdateReleaseRequest
		expectedErrMsg string
	}{
		{
			name: "all_valid_fields",
			args: []string{
				"--name", "name", "--description", "desc", "--milestone", "m1", "--milestone", "m2",
				"--released-at", "2022-05-30T21:10:23.284Z",
			},
			expected: &baseReq,
		},
		{
			name: "no_name",
			args: []string{
				"--description", "desc", "--milestone", "m1", "--milestone", "m2",
				"--released-at", "2022-05-30T21:10:23.284Z",
			},
			expected: newTestUpdateReleaseRequest(baseReq, func(req *gitlab.UpdateReleaseRequest) {
				req.Name = ""
			}),
		},
		{
			name: "no_released_at",
			args: []string{"--name", "name", "--description", "desc", "--milestone", "m1", "--milestone", "m2"},
			expected: newTestUpdateReleaseRequest(baseReq, func(req *gitlab.UpdateReleaseRequest) {
				req.ReleasedAt = nil
			}),
		},
		{
			name: "no_milestones",
			args: []string{"--name", "name", "--description", "desc", "--released-at", "2022-05-30T21:10:23.284Z"},
			expected: newTestUpdateReleaseRequest(baseReq, func(req *gitlab.UpdateReleaseRequest) {
				req.Milestones = make([]string, 0)
			}),
		},
		{
			name: "empty_milestone",
			args: []string{
				"--name", "name", "--description", "desc", "--released-at", "2022-05-30T21:10:23.284Z",
				"--milestone", "",
			},
			expected: newTestUpdateReleaseRequest(baseReq, func(req *gitlab.UpdateReleaseRequest) {
				req.Milestones = []string{""}
			}),
		},
		{
			name: "no_extra_args",
			args: []string{},
			expected: newTestUpdateReleaseRequest(baseReq, func(req *gitlab.UpdateReleaseRequest) {
				req.Name = ""
				req.Description = ""
				req.Milestones = make([]string, 0)
				req.ReleasedAt = nil
			}),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			args := append(requiredArgs, tt.args...)
			set := getFlagSet(t, args...)
			ctx := cli.NewContext(nil, set, nil)

			got, err := newUpdateReleaseReq(ctx, logger)
			if tt.expectedErrMsg != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.expectedErrMsg)
				return
			}
			require.NoError(t, err)

			require.Equal(t, tt.expected, got)
		})
	}
}

func newTestUpdate(t *testing.T, logger logrus.FieldLogger, mhc gitlab.HTTPClient, extraArgs ...string) (*cli.Command, *cli.Context) {
	t.Helper()

	clientFn := func(ctx *cli.Context, log logrus.FieldLogger) (gitlab.HTTPClient, error) {
		return mhc, nil
	}

	args := []string{"update"}
	args = append(args, extraArgs...)

	app := &cli.App{Writer: io.Discard}
	set := flag.NewFlagSet(t.Name(), 0)
	set.String(flags.ServerURL, "https://gitlab.com", "")
	set.String(flags.JobToken, "token", "")

	err := set.Parse(args)
	require.NoError(t, err)

	update := Update(logger, clientFn)
	ctx := cli.NewContext(app, set, nil)

	return update, ctx
}

func newTestUpdateReleaseRequest(base gitlab.UpdateReleaseRequest, mutators ...func(*gitlab.UpdateReleaseRequest)) *gitlab.UpdateReleaseRequest {
	for _, mutate := range mutators {
		mutate(&base)
	}

	return &base
}
